<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

use App\User;

class UserController extends Controller
{
    public $validation = ['name' => 'required', 'email' => 'required'];

    public function index() {
    	$users = User::get();
    	return view('admin.users.index', compact('users'));
    }

    public function create() {
		return view('admin.users.create');
	}

    public function store(Request $request) {
    	$this->validate($request, $this->validation);
		$user = User::create($request->all());

		if ($request->password) {
			$user->update(['password' => Hash::make($request->password)]);
		}

		return redirect()->route('admin.users.edit', $user->id)->with([
			'alert' => 'User created.'
		]);
	}

	public function edit(User $user) {
		return view('admin.users.edit', compact('user'));
    }

    public function update(User $user, Request $request) {
		$this->validate($request, $this->validation);
		$user->update($request->except(['password']));

		if ($request->password) {
			$user->update(['password' => Hash::make($request->password)]);
		}

		return redirect()->route('admin.users.edit', $user->id)->with([
			'alert' => 'User saved.'
		]);
	}

	public function destroy(User $user, Request $request) {
		$user->delete();
		return redirect()->back()->with(['alert' => 'User deleted.']);
	}
}
