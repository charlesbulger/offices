<?php

namespace App\Http\Middleware;

use Closure;

use App\Page;

class IsPage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        dd('hello');

        $page = Page::where('slug', $request->path())->first();
        if ($page) {
            return response(view('main.page', compact('page')));
        }

        return $next($request);
    }
}
