<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $fillable = ['state_id', 'slug', 'name'];

    public function state() {
    	return $this->belongsTo('App\State');
    }

    public function businesses() {
    	return $this->hasMany('App\Business');
    }

    public static function lists() {
        return self::orderBy('name')->pluck('name', 'id')->all();
    }

    public function getLinkAttribute() {
        return url($this->state->slug . '/' . $this->slug);
    }
}
