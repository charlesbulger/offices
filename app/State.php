<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $fillable = ['slug', 'abbreviation', 'name'];

    public function cities() {
    	return $this->hasMany('App\City');
    }

    public function businesses() {
        return $this->hasManyThrough('App\Business', 'App\City');
    }

    public static function lists() {
        return self::orderBy('name')->pluck('name', 'id')->all();
    }

    public function getLinkAttribute() {
        return url($this->slug);
    }
}
