<fieldset>
	{{ Form::label('state_id', 'State') }}
	{{ Form::select('state_id', [null => 'Select'] + $states, null, ['required' => true]) }}
</fieldset>

<fieldset>
	{{ Form::label('name', 'Name') }}
	{{ Form::text('name', null, ['required' => true, 'class' => 'slugify']) }}
</fieldset>

<fieldset>
	{{ Form::label('slug', 'Slug') }}
	{{ Form::text('slug', null, ['required' => true]) }}
</fieldset>

{{ Form::submit('Save') }}