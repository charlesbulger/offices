<table>
	<thead>
		<th>Name</th>
		<th>Slug</th>
		<th>State</th>
		<th></th>
	</thead>
	<tbody>
		@foreach ($cities as $city)
			<tr>
				<td><a href="{{ route('admin.cities.edit', $city->id) }}">{{ $city->name }}</a></td>
				<td>{{ $city->slug }}</td>
				<td>{{ $city->state->name }}</td>
				<td>
					<a href="{{ route('admin.cities.edit', $city->id ) }}">Edit</a> | 
					<a class="delete-link" href="javascript:;">Delete</a>
					{{ Form::open(['method' => 'DELETE', 'route' => ['admin.cities.destroy', $city->id]]) }} {{ Form::close() }}
				</td>
			</tr>
		@endforeach
	</tbody>
</table>