@extends('admin.app', ['title' => 'Create a Page'])



@section('content')
	<h1><a href="{{ route('admin.pages.index') }}">Pages</a> / Create</h1>
	{{ Form::open(['route' => 'admin.pages.store', 'class' => 'validate']) }}
		@include('admin.pages._form')
	{{ Form::close() }}
@stop