<table>
	<thead>
		<th>Title</th>
		<th></th>
	</thead>
	<tbody>
		@foreach ($pages as $page)
			<tr>
				<td>
					<a href="{{ route('admin.pages.edit', $page->id) }}">{{ $page->title }}</a><br>
					<span class="small"><a target="_blank" href="{{ url($page->slug) }}">{{ $page->slug }} <i class="fa fa-external-link"></i></a></span>
				</td>
				<td>
					<a target="_blank" href="{{ url($page->slug) }}">View <i class="fa fa-external-link"></i></a> |
					<a href="{{ route('admin.pages.edit', $page->id ) }}">Edit</a> | 
					<a class="delete-link" href="javascript:;">Delete</a>
					{{ Form::open(['method' => 'DELETE', 'route' => ['admin.pages.destroy', $page->id]]) }} {{ Form::close() }}
				</td>
			</tr>
		@endforeach
	</tbody>
</table>