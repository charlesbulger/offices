<fieldset>
	{{ Form::label('slug', 'Slug') }}
	{{ Form::text('slug', null, ['required' => true]) }}
</fieldset>

<fieldset>
	{{ Form::label('title', 'Title') }}
	{{ Form::text('title', null, ['required' => true]) }}
</fieldset>

{{ Form::submit('Save') }}