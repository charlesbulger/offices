@extends('admin.app', ['title' => 'Pages'])




@section('content')
	<a class="btn float-right" href="{{ route('admin.pages.create') }}">Create Page</a>
	<h1>Pages</h1>
	@include('admin.pages._table', ['pages' => $pages])
@stop