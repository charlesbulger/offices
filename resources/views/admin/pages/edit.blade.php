@extends('admin.app', ['title' => 'Edit Page: ' . $page->slug])



@section('content')
	<h1><a href="{{ route('admin.pages.index') }}">Pages</a> / Edit Page: <span data-mirror="slug">{{ $page->slug }}</span></h1>
	{{ Form::model($page, ['route' => ['admin.pages.update', $page->id], 'method' => 'PATCH', 'class' => 'validate']) }}
		@include('admin.pages._form')
	{{ Form::close() }}
@stop