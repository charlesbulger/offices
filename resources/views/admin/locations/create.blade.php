@extends('admin.app', ['title' => 'Create'])



@section('content')
	<h1><a href="{{ route('admin.locations.index') }}">Locations</a> / Create</h1>
	{{ Form::open(['route' => 'admin.locations.store', 'class' => 'validate']) }}
		@include('admin.locations._form')
	{{ Form::close() }}
@stop