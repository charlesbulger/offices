@extends('admin.app', ['title' => 'Businesses'])




@section('content')
	<a class="btn float-right" href="{{ route('admin.businesses.create') }}">Create Business</a>
	<h1>Businesses</h1>
	@include('admin.businesses._table', ['businesses' => $businesses])
@stop