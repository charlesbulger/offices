<table>
	<thead>
		<th>Name</th>
		<th>Slug</th>
		<th></th>
	</thead>
	<tbody>
		@foreach ($states as $state)
			<tr>
				<td><a href="{{ route('admin.states.edit', $state->id) }}">{{ $state->name }}</a></td>
				<td>{{ $state->slug }}</td>
				<td>
					<a href="{{ route('admin.states.edit', $state->id ) }}">Edit</a> | 
					<a class="delete-link" href="javascript:;">Delete</a>
					{{ Form::open(['method' => 'DELETE', 'route' => ['admin.states.destroy', $state->id]]) }} {{ Form::close() }}
				</td>
			</tr>
		@endforeach
	</tbody>
</table>