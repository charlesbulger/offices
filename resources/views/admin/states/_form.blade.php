<fieldset>
	{{ Form::label('name', 'Name') }}
	{{ Form::text('name', null, ['required' => true]) }}
</fieldset>

<fieldset>
	{{ Form::label('slug', 'Slug') }}
	{{ Form::text('slug', null, ['required' => true]) }}
</fieldset>

{{ Form::submit('Save') }}