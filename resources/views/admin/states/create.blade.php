@extends('admin.app', ['title' => 'Create a State'])



@section('content')
	<h1><a href="{{ route('admin.states.index') }}">States</a> / Create</h1>
	{{ Form::open(['route' => 'admin.states.store', 'class' => 'validate']) }}
		@include('admin.states._form')
	{{ Form::close() }}
@stop