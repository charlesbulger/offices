<table>
	<thead>
		<th>Name</th>
		<th>Email</th>
		<th></th>
	</thead>
	<tbody>
		@foreach ($users as $user)
			<tr>
				<td><a href="{{ route('admin.users.edit', $user->id) }}">{{ $user->name }}</a></td>
				<td>{{ $user->email }}</td>
				<td>
					<a href="{{ route('admin.users.edit', $user->id ) }}">Edit</a> |
					<a class="delete-link" href="{{ route('admin.users.edit', $user->id ) }}">Delete</a>
					{{ Form::open(['method' => 'DELETE', 'route' => ['admin.users.destroy', $user->id]]) }} {{ Form::close() }}
				</td>
			</tr>
		@endforeach
	</tbody>
</table>