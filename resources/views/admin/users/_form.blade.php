<fieldset>
	{{ Form::label('name', 'Name') }}
	{{ Form::text('name') }}
</fieldset>

<fieldset>
	{{ Form::label('email', 'Email') }}
	{{ Form::text('email') }}
</fieldset>

<fieldset>
	{{ Form::label('password', 'Password') }}
	{{ Form::password('password', null) }}
</fieldset>

{{ Form::submit('Save') }}