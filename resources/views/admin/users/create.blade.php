@extends('admin.app', ['title' => 'Create a User'])


@section('content')
	<h1><a href="{{ route('admin.users.index') }}">Users</a> / Create</h1>
	{{ Form::open(['route' => 'admin.users.store']) }}
		@include('admin.users._form')
	{{ Form::close() }}
@stop