@extends('admin.app', ['title' => 'Users'])



@section('content')
	<a class="btn float-right" href="{{ route('admin.users.create') }}">Create User</a>
	<h1>Users</h1>
	@include('admin.users._table', ['users' => $users])
@stop