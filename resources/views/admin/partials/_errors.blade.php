@if ($errors->any())
	<div class="alert alert-error">
		<a href="javascript:;" class="alert-close"><i class="fa fa-close"></i></a>
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div><!-- alert error -->
@endif