@extends('admin.app')

@section('content')

    <h1>Login</h1>

    {{ Form::open(['url' => 'login', 'class' => 'validate']) }}

        {{ Form::label('email', 'Email') }}
        {{ Form::email('email', null, ['required' => true]) }}

        {{ Form::label('password', 'Password') }}
        {{ Form::password('password', ['required' => true]) }}

        {{ Form::submit('Login') }}
        <a href="{{ route('password.request') }}">Forgot Your Password?</a>

    {{ Form::close() }}

@endsection