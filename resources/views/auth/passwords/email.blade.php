@extends('admin.app')



@section('content')

    <h1>Reset Password</h1>

    {{ Form::open(['route' => 'password.email', 'class' => 'validate']) }}

        {{ Form::label('email', 'Email') }}
        {{ Form::email('email', null, ['required' => true]) }}


        {{ Form::submit('Send Password Reset Link') }}

    {{ Form::close() }}

@endsection