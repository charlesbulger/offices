<h3>Showing {{ $locations->firstItem() }} to {{ $locations->lastItem() }} of {{ $locations->total() }} Locations</h3>
@foreach ($locations as $preview)
	@include('main.locations.partials.preview', ['location' => $preview])
@endforeach
{{ $locations->appends(request()->except('page'))->links() }}