<!doctype html>
<html>
	<head>
		<title>{{ isset($title) ? $title : 'Branch' }}</title>

		<!-- meta -->
		<meta charset="utf-8">
		<base href="<?= url('/'); ?>/">
		<link rel="shortcut icon" href="static/main/images/favicon.png">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="robots" content="noindex">

		<!-- css -->
		<link rel="stylesheet" type="text/css" href="static/main/css/style.css?r={{ rand(1,999) }}">
		<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400,700">

		<!-- csrf -->
		<meta name="csrf-token" content="{{ csrf_token() }}">
		
		<!--[if lt IE 9]>
			<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
		<![endif]-->
	</head>
	<body>
		
		<header>
			<div class="c">
				<a href="{{ url('/') }}" class="logo">County<span>Offices</span></a>
				{{ Form::open() }}
					{{ Form::text('search', null, ['placeholder' => 'Search City, State, or Zip']) }}
					{{ Form::submit('Search') }}
				{{ Form::close() }}
			</div><!--c -->
		</header>
		@include('main.partials.breadcrumbs')
		<div class="main">
			<div class="c">	
				
				@yield('content')
			</div><!--c -->
		</div><!-- main -->	
		<footer>
			<div class="c">
				<a href="{{ url('/') }}" class="logo">County<span>Offices</span></a>
				<ul class="nav">
					<li><a href="">About Us</a></li>
					<li><a href="">Contact Us</a></li>
					<li><a href="">Terms of Use</a></li>
					<li><a href="">Privacy Policy</a></li>
					<li><a href="">Data</a></li>
				</ul>
				<p class="copyright">Copyright &copy; 2018 Branchspot</p>
			</div><!-- c -->
		</footer>

		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>
		<script type="text/javascript" src="static/main/js/app.js"></script>
	</body>
</html>