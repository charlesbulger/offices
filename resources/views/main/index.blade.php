@extends('main.app', ['title' => 'Location'])


@section('content')
	<div class="grid">
		<div class="col-2-3">
			<h1>Home Page!</h1>
			@include('main.locations.partials.list')
		</div><!-- col-2-3 -->
		<div class="col-1-3">
			@include('main.partials.map')
			@include('main.partials.adsense')
		</div><!-- col-1-3 -->
	</div><!-- grid -->
@stop