@if (isset($locations) && count($locations) > 0)
  <div class="map"><div id="map"></div></div>
  <script>
  	function initMap() {
      var map = new google.maps.Map(document.getElementById('map'), {
        	zoom: 5,
        	center: {lat: {{ $locations[0]->lat }} , lng: {{ $locations[0]->lng }} }
      });
      var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    	var markers = locations.map(function(location, i) {
      		return new google.maps.Marker({
        		position: location,
        		label: labels[i % labels.length]
      		});
    	});
    	var markerCluster = new MarkerClusterer(map, markers, {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
  	}

  	var locations = [
  		@foreach ($locations as $location)
  			{lat: {{ $location->lat }}, lng: {{ $location->lng }} },
  		@endforeach
  	]
  </script>
  <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAs03aWH-P2ih7cDBWQIoP213kyKB85Jes&callback=initMap"></script>
@endif