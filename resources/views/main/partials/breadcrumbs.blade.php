<div class="breadcrumbs">
	<div class="c">
		<ul>
			<li><a href="{{ url('/') }}"><i class="fa fa-home"></i> Home</a></li>
			@if (isset($state))
				<li><a href="{{ $state->link }}">{{ $state->name }}</a></li>
				@if (isset($city))
					<li><a href="{{ $city->link }}">{{ $city->name }}</a></li>
					@if (isset($business))
						<li><a href="{{ $business->link }}">{{ $business->name }}</a></li>
						@if (isset($location))
							<li><a href="{{ $location->link }}">{{ $location->address }}</a></li>
						@endif
					@endif
				@endif
			@elseif (isset($page))
				<li><a href="{{ $page->link }}">{{ $page->title }}</a></li>
			@endif
		</ul>
	</div><!-- c -->
</div><!-- breadcrumbs -->