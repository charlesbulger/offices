$.ajaxSetup({ headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});

$(document).ready(function() {

	// Close Alert
	////////////////////////////////////////////////////
	$(".alert-close").click(function(e) {
		e.preventDefault();
		$(this).parents(".alert").slideUp(200);
	});

	// Close Alert
	////////////////////////////////////////////////////
	$('.validate').each(function() {
        $(this).validate({
	 		errorElement: "div",
	 		rules: {
	 			phone: { phoneUS: true },
	 			email: { email: true }
	 		}
		});
    });


});